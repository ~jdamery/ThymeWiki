|    | Lunch                  | Dinner    | Notes
|---:|:-----------------------|:----------|:------
 Tue | Sammich                |  Gousto\* | Lamb Curry
 Wed | Sammich                |  Gousto\* | Chicken Linguine
 Thu | Sammich                |  Gousto\* | One Pot Pasta
 Fri | Sammich                |  Gousto\* | Curry in a Hurry
 Sat | Sammich                |  Gousto\* | Cajun Fish
 Sun | Gousto\*               |  Pie      | Glazed Sausages
 Mon | Sammich                |  Pizza* / S Food

 Items marked with a * have been purchased.
